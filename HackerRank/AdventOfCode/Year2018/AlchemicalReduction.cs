﻿using System;
using System.Collections.Generic;

namespace HackerRank.AdventOfCode.Year2018
{
	class AlchemicalReduction
	{
		public static List<char> React(List<char> inputPolymer)
		{
			List<char> polymer = inputPolymer;
			int lastPolymerLength;
			do
			{
				lastPolymerLength = polymer.Count;

				// React all units in the polymer until no reactions remain
				for (int i = 0; i < polymer.Count - 1; i++)
				{
					char unitA = polymer[i];
					char unitB = polymer[i + 1];
					bool polarityDifference = Char.IsUpper(unitA) ^ Char.IsUpper(unitB);

					// Do unit reactions
					if (polarityDifference && (Char.ToLower(unitA) == Char.ToLower(unitB)))
					{
						polymer.RemoveRange(i, 2);
					}
				}
			}
			while (lastPolymerLength != polymer.Count);

			return polymer;
		}

		public static void Main()
		{
			string input = @"";

			List<char> polymer = new List<char>(input);
			polymer = React(polymer);

			Console.WriteLine("Part 1: " + polymer.Count);

			string unitTypes = @"abcdefghijklmnopqrstuvwxyz";
			int shortestLength = Int32.MaxValue;
			char unitOfShortestLength = '\0';
			for(int i=0; i<unitTypes.Length; i++)
			{
				// Remove units of both polarities
				string improvedInput = input.Replace(unitTypes[i].ToString().ToLower(), "");
				improvedInput = improvedInput.Replace(unitTypes[i].ToString().ToUpper(), "");

				polymer = new List<char>(improvedInput);
				polymer = React(polymer);

				// Track the shortest polymer
				if(polymer.Count < shortestLength)
				{
					shortestLength = polymer.Count;
					unitOfShortestLength = unitTypes[i];
				}
			}
			Console.WriteLine("Part 2: " + shortestLength);
		}
	}
}