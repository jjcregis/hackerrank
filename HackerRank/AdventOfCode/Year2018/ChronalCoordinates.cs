﻿using System;
using System.Collections.Generic;

namespace HackerRank.AdventOfCode.Year2018
{
	class ChronalCoordinates
	{
		public static void Main()
		{
			const int EQUAL_DISTANCE = Int32.MaxValue;

			string input = @"";

			// Read input and parse coordinates
			string[] lines = input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
			List<Tuple<int, int>> coordinates = new List<Tuple<int, int>>();

			for(int i=0; i<lines.Length; i++)
			{
				string[] lineTokens = lines[i].Split(',');
				Tuple<int, int> coordinate = new Tuple<int, int>(Int32.Parse(lineTokens[0]), Int32.Parse(lineTokens[1]));
				coordinates.Add(coordinate);
			}

			// Minimise search area by finding min and max coordinates in each direction
			int xMin = Int32.MaxValue;
			int xMax = Int32.MinValue;
			int yMin = Int32.MaxValue;
			int yMax = Int32.MinValue;
			for (int i=0; i<coordinates.Count; i++)
			{
				if(coordinates[i].Item1 < xMin)
				{
					xMin = coordinates[i].Item1;
				}
				else if (coordinates[i].Item1 > xMax)
				{
					xMax = coordinates[i].Item1;
				}

				if (coordinates[i].Item2 < yMin)
				{
					yMin = coordinates[i].Item2;
				}
				else if (coordinates[i].Item2 > yMax)
				{
					yMax = coordinates[i].Item2;
				}
			}
			int mapXSize = xMax - xMin;
			int mapYSize = yMax - yMin;
			int[][] coverage = new int[mapXSize][];
			for (int i = 0; i < mapXSize; i++)
			{
				coverage[i] = new int[mapYSize];
			}

			// For each cell, find which coordinate is closest
			for (int x = 0; x < mapXSize; x++)
			{
				int realX = x + xMin;
				for (int y = 0; y < mapYSize; y++)
				{
					int realY = y + yMin;
					int lowestDistance = Int32.MaxValue;
					for (int i=0; i<coordinates.Count; i++)
					{
						// Add the cropped distances back into distance calculations
						int distance = Math.Abs(realX - coordinates[i].Item1) + Math.Abs(realY - coordinates[i].Item2);
						if(distance < lowestDistance)
						{
							lowestDistance = distance;

							// Save the index of the closest coordinate
							coverage[x][y] = i;
						}
						else if (distance == lowestDistance)
						{
							coverage[x][y] = EQUAL_DISTANCE;
						}
					}
				}
			}

			// Find the areas nearest each coordinate
			int[] areas = new int[coordinates.Count];
			for (int x = 0; x < mapXSize; x++)
			{
				for (int y = 0; y < mapYSize; y++)
				{
					if (coverage[x][y] != EQUAL_DISTANCE)
					{
						areas[coverage[x][y]]++;						
					}
				}
			}

			// Find the largest area
			int largestArea = 0;
			for(int i=0; i<areas.Length; i++)
			{
				if (areas[i] > largestArea)
				{
					largestArea = areas[i];
				}
			}
			Console.WriteLine("Part 1: " + largestArea); //TODO: Need to eliminate infinite areas

			// Find the distance of each cell to each coordinate
			for (int x = 0; x < mapXSize; x++)
			{
				int realX = x + xMin;
				for (int y = 0; y < mapYSize; y++)
				{
					int realY = y + yMin;
					int totalDistance = 0;
					for (int i = 0; i < coordinates.Count; i++)
					{
						totalDistance += Math.Abs(realX - coordinates[i].Item1) + Math.Abs(realY - coordinates[i].Item2);
					}
					coverage[x][y] = totalDistance;
				}
			}

			// Find the size of the region with the best coverage
			int area = 0;
			for (int x = 0; x < mapXSize; x++)
			{
				for (int y = 0; y < mapYSize; y++)
				{
					if(coverage[x][y] < 10000)
					{
						area++;
					}
				}
			}
			Console.WriteLine("Part 2: " + area);
		}
	}
}