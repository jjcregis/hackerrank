﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
	class HalloweenParty
	{
		public static void Main()
		{
			int numberOfTestCases = Convert.ToInt32(Console.ReadLine());

			for (int i = 0; i < numberOfTestCases; i++)
			{
				long numberOfCuts = Convert.ToInt64(Console.ReadLine());

				if (numberOfCuts % 2 == 0)
				{
					Console.WriteLine((numberOfCuts / 2) * (numberOfCuts / 2));
				}
				else
				{
					Console.WriteLine((numberOfCuts / 2) * (numberOfCuts / 2 + 1));
				}
			}
		}
	}
}