﻿using System;
using System.Collections.Generic;

namespace HackerRank.AdventOfCode.Year2018
{
	class ChronalCalibration
	{
		public static void Main()
		{
			string input = @"";

			// Parse input
			string[] splitInput = input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
			List<int> frequencies = new List<int>();
			foreach (string line in splitInput)
			{
				int sign = 1; // Positive or negative sign
				if (line[0] == '+')
				{
					// Do nothing
				}
				else if (line[0] == '-')
				{
					sign = -1;
				}
				else
				{
					continue;
				}
				frequencies.Add(Int32.Parse(line.Substring(1)) * sign);
			}

			// Calculate total frequency
			int totalFrequency = 0;
			foreach (int frequency in frequencies)
			{
				totalFrequency += frequency;
			}

			Console.WriteLine("Part 1: " + totalFrequency);

			// Find first duplicate frequency
			HashSet<int> foundFrequencies = new HashSet<int>();
			bool duplicateFound = false;
			totalFrequency = 0;
			while (!duplicateFound) {
				foreach (int frequency in frequencies)
				{
					totalFrequency += frequency;
					if (!foundFrequencies.Contains(totalFrequency))
					{
						foundFrequencies.Add(totalFrequency);
					}
					else
					{
						duplicateFound = true;
						break;
					}
				}
			}

			Console.WriteLine("Part 2: " + totalFrequency);
		}
	}
}