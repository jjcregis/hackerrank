﻿using System;

namespace HackerRank.Problems.Algorithms
{
	class ChocolateFeast
	{
        private struct TestCaseInfo
        {
            public int money;
            public int cost;
            public int wrapperTradeInCost;
        }

		public static void Main()
		{
			int numberOfTestCases = Convert.ToInt32(Console.ReadLine());
            TestCaseInfo[] testCases = new TestCaseInfo[numberOfTestCases];

            for (int i = 0; i < numberOfTestCases; i++)
            {
                string[] testCaseInfoAsString = Console.ReadLine().Split(' ');
                testCases[i].money = Convert.ToInt32(testCaseInfoAsString[0]);
                testCases[i].cost = Convert.ToInt32(testCaseInfoAsString[1]);
                testCases[i].wrapperTradeInCost = Convert.ToInt32(testCaseInfoAsString[2]);
            }

            for (int i = 0; i < testCases.Length; i++)
			{
                int money = testCases[i].money;
                int cost = testCases[i].cost;
                int wrapperTradeInCost = testCases[i].wrapperTradeInCost;

                int numberOfChocolates = money / cost;
                int numberOfWrappers = numberOfChocolates;
                do
                {
                    // Get new chocolates with wrappers
                    int numberOfNewChocolates = numberOfWrappers / wrapperTradeInCost;
                    numberOfWrappers %= wrapperTradeInCost;
                    
                    numberOfWrappers += numberOfNewChocolates;
                    numberOfChocolates += numberOfNewChocolates;
                }
                while (numberOfWrappers >= wrapperTradeInCost);

				Console.WriteLine(numberOfChocolates);
			}
		}
	}
}