﻿using System;

namespace HackerRank.Problems.Algorithms
{
    class MiniMaxSum
    {
        public static void Main()
        {
            string[] longsAsStrings = Console.ReadLine().Split(' ');
            long[] longs = new long[longsAsStrings.Length];
            int leastIndex = 0;
            int greatestIndex = 0;

            // Convert and get least and greatest numbers
            for (int i=0; i<longsAsStrings.Length; i++)
            {
                longs[i] = Convert.ToInt64(longsAsStrings[i]);
                if (longs[i] < longs[leastIndex])
                {
                    leastIndex = i;
                }
                if (longs[i] > longs[greatestIndex])
                {
                    greatestIndex = i;
                }
            }
            
            // Get the least and greatest sums by omitting the greatest and least numbers, respectively
            long leastSum = 0;
            long greatestSum = 0;
            for (int i=0; i<longs.Length; i++)
            {
                if (i != greatestIndex)
                {
                    leastSum += longs[i];
                }
                if (i != leastIndex)
                {
                    greatestSum += longs[i];
                }
            }

            Console.WriteLine(leastSum + " " + greatestSum);
        }
    }
}