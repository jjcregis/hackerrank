﻿using System;
using System.Numerics;

namespace HackerRank.Problems.Algorithms
{
    class ExtraLongFactorials
    {

        public static void Main()
        {
            int number = Convert.ToInt32(Console.ReadLine());
            BigInteger[] factorials = new BigInteger[number + 1];
            factorials[1] = 1;

            for(int i=2; i<=number; i++)
            {
                factorials[i] = i * factorials[i - 1];
            }

            Console.WriteLine(factorials[number]);
        }
    }
}