﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
	class AngryProfessor
	{
		public static void Main()
		{
			int numberOfTestCases = Convert.ToInt32(Console.ReadLine());

			for (int i = 0; i < numberOfTestCases; i++)
			{
				string[] testCaseInfoAsString = Console.ReadLine().Split(' ');
				int onTimeStudentsNeeded = Convert.ToInt32(testCaseInfoAsString[1]);

				string[] studentTimesAsString = Console.ReadLine().Split(' ');
				for (int j = 0; j < studentTimesAsString.Length; j++ )
				{
					int time = Convert.ToInt32(studentTimesAsString[j]);

					if(time <= 0)
					{
						onTimeStudentsNeeded--;
					}
				}

				if(onTimeStudentsNeeded <= 0)
				{
					Console.WriteLine("NO");
				}
				else
				{
					Console.WriteLine("YES");
				}
			}
		}
	}
}