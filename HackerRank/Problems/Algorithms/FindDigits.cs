﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
	class FindDigits
	{
		public static void Main()
		{
			int numberOfTestCases = Convert.ToInt32(Console.ReadLine());

			for (int i = 0; i < numberOfTestCases; i++)
			{
				string numberAsString = Console.ReadLine();
				long number = Convert.ToInt64(numberAsString);
				int evenlyDividingDigits = 0;

				for (int j = 0; j < numberAsString.Length; j++)
				{
					int digit = Convert.ToInt32(numberAsString[j] - '0');
					if (digit != 0 && number % digit == 0)
					{
						evenlyDividingDigits++;
					}
				}

				Console.WriteLine(evenlyDividingDigits);
			}
		}
	}
}