﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
    class SolveMeSecond
    {
		public static void Main()
        {
            int numberOfTestCases = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < numberOfTestCases; i++)
            {
                string[] numbersAsString = Console.ReadLine().Split(' ');

                int a = Convert.ToInt32(numbersAsString[0]);
                int b = Convert.ToInt32(numbersAsString[1]);
                Console.WriteLine(a + b);
            }
        }
    }
}