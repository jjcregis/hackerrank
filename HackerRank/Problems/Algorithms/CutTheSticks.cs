﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
	class CutTheSticks
	{
		public static void Main()
		{
			Console.ReadLine();
			string[] sticksAsString = Console.ReadLine().Split(' ');

			List<int> sticks = new List<int>();
			for (int i = 0; i < sticksAsString.Length; i++)
			{
				sticks.Add(Convert.ToInt32(sticksAsString[i]));
			}

			while(sticks.Count > 0)
			{
				Console.WriteLine(sticks.Count);

				sticks.Sort();
				int shortestStick = sticks[0];

				// Cut sticks
				for (int i = 0; i < sticks.Count; i++)
				{
					sticks[i] -= shortestStick;
				}

				// Remove destroyed sticks
				while(sticks.Count > 0 && sticks[0] <= 0)
				{
					sticks.RemoveAt(0);
				}
			}
		}
	}
}