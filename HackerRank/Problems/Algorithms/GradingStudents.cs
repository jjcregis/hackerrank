﻿using System;
using System.Collections.Generic;

namespace HackerRank.Problems.Algorithms
{
	class GradingStudents
	{
		public static void Main()
		{
			int numberOfStudents = Convert.ToInt32(Console.ReadLine());
			var grades = new List<int>();
			for (int i =0; i<numberOfStudents; i++)
			{
				int grade = Convert.ToInt32(Console.ReadLine());

				// Round the grade
				if(grade >= 38)
				{
					var distanceFromNextGrade = 5 - (grade % 5);
					if(distanceFromNextGrade < 3)
					{
						grade += distanceFromNextGrade;
					}					
				}
				grades.Add(grade);				
			}

			foreach(var grade in grades)
			{
				Console.WriteLine(grade);
			}
		}
	}
}