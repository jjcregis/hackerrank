﻿using System;

namespace HackerRank.AdventOfCode.Year2019
{
	class SpaceImageFormat
	{
		public static void Main()
		{
			string input = @"";
			int width = 25;
			int height = 6;

			// Read input
			string[] lines = input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

			// Create 3D array of image data
			int numberOfLayers = lines[0].Length / (width * height);
			int[][][] data = new int[width][][];
			for (int i = 0; i < width; i++)
			{
				data[i] = new int[height][];
				for (int j = 0; j < height; j++)
				{
					data[i][j] = new int[numberOfLayers];
				}
			}

			// Fill array with data
			for (int i = 0; i < lines[0].Length; i++)
			{
				int digit = lines[0][i] - '0';
				data[i % width][i / width % height][i / (width * height) % numberOfLayers] = digit;
			}

			// Find the later with the fewest zeroes
			int fewestDigitsLayer = int.MinValue;
			int fewestDigitsCount = int.MaxValue;
			for (int l = 0; l < numberOfLayers; l++)
			{
				int digitsCount = 0;
				for (int x = 0; x < width; x++)
				{
					for (int y = 0; y < height; y++)
					{
						if (data[x][y][l] == 0)
						{
							digitsCount++;
						}
					}
				}

				if (digitsCount < fewestDigitsCount)
				{
					fewestDigitsCount = digitsCount;
					fewestDigitsLayer = l;
				}
			}

			// Get answer from that layer
			int onesCount = 0;
			int twosCount = 0;
			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					switch (data[x][y][fewestDigitsLayer])
					{
						case 1:
							onesCount++;
							break;
						case 2:
							twosCount++;
							break;
					}
				}
			}
			Console.WriteLine($"Part 1: {onesCount * twosCount}");

			// Decode final image
			int[][] finalImage = new int[width][];
			for (int i = 0; i < width; i++)
			{
				finalImage[i] = new int[height];
			}

			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					for (int l = 0; l < numberOfLayers; l++)
					{
						if (data[x][y][l] != 2)
						{
							finalImage[x][y] = data[x][y][l];
							break;
						}
					}
				}
			}

			// Print final image
			Console.WriteLine("Part 2:");
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					char character = 'X';
					switch (finalImage[x][y])
					{
						case 0:
							character = '█';
							break;
						case 1:
							character = '░';
							break;
						case 2:
							character = ' ';
							break;
					}
					Console.Write(character);
				}
				Console.WriteLine();
			}
		}
	}
}
