﻿using System;

namespace HackerRank.Problems.Algorithms
{
    class CompareTheTriplets
    {
        public static void Main()
        {
            string[] tripletA = Console.ReadLine().Split(' ');
            string[] tripletB = Console.ReadLine().Split(' ');
            int pointsA = 0;
            int pointsB = 0;

            for(int i=0; i<3; i++)
            {
                if (Convert.ToInt32(tripletA[i]) > Convert.ToInt32(tripletB[i]))
                {
                    pointsA++;
                }

                else if (Convert.ToInt32(tripletA[i]) < Convert.ToInt32(tripletB[i]))
                {
                    pointsB++;
                }
            }

            Console.WriteLine(pointsA + " " + pointsB);
        }
    }
}