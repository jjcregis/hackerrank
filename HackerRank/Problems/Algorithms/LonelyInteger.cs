﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
	class LonelyInteger
	{
		public static void Main()
		{
			int numberOfInts = Convert.ToInt32(Console.ReadLine());
			string[] intsAsStrings = Console.ReadLine().Split(' ');

			HashSet<int> intsFound = new HashSet<int>();
			
			// When an int is found, add it to the set. If it is found a second time, remove it.
			// The remaining int in the set is the lone integer.
			for (int i = 0; i < numberOfInts; i++)
			{
				int newInt = Convert.ToInt32(intsAsStrings[i]);

				if(intsFound.Contains(newInt))
				{
					intsFound.Remove(newInt);
				}
				else
				{
					intsFound.Add(newInt);
				}
			}
			foreach(int i in intsFound)
			{
				Console.WriteLine(i);
			}
			
		}
	}
}