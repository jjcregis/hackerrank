﻿using System;
using System.Collections.Generic;

namespace HackerRank.AdventOfCode.Year2018
{
	class NoMatterHowYouSliceIt
	{
		public static void Main()
		{
			const int MAX_SIZE = 1000;

			string input = @"";

			// Track coverage
			int[][] coverage = new int[MAX_SIZE][];
			for (int i=0; i< MAX_SIZE; i++)
			{
				coverage[i] = new int[MAX_SIZE];
			}
			HashSet<Tuple<int, int>> coverageOfTwo = new HashSet<Tuple<int, int>>();

			// Read input and parse tokens on each lines
			string[] splitInput = input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
			string[][] lineTokens = new string[splitInput.Length][];
			for(int i=0; i< splitInput.Length; i++)
			{
				lineTokens[i] = splitInput[i].Split(new char[] { '#', ' ', '@', ',', ':', 'x' }, StringSplitOptions.RemoveEmptyEntries);
			}

			for(int i=0; i< splitInput.Length; i++)
			{
				int id = Int32.Parse(lineTokens[i][0]);
				int indentX = Int32.Parse(lineTokens[i][1]);
				int indentY = Int32.Parse(lineTokens[i][2]);
				int width = Int32.Parse(lineTokens[i][3]);
				int height = Int32.Parse(lineTokens[i][4]);

				for(int x=indentX; x<indentX+width; x++)
				{
					for(int y=indentY; y<indentY+height; y++)
					{
						// Increment coverage of each cell.
						coverage[x][y]++;
						if(coverage[x][y] >= 2 && !coverageOfTwo.Contains(new Tuple<int, int>(x, y)))
						{
							// Track cells with enough overlap
							coverageOfTwo.Add(new Tuple<int, int>(x, y));
						}
					}
				}
			}
			Console.WriteLine("Part 1: " + coverageOfTwo.Count);

			for (int i = 0; i < MAX_SIZE; i++)
			{
				int id = Int32.Parse(lineTokens[i][0]);
				int indentX = Int32.Parse(lineTokens[i][1]);
				int indentY = Int32.Parse(lineTokens[i][2]);
				int width = Int32.Parse(lineTokens[i][3]);
				int height = Int32.Parse(lineTokens[i][4]);

				bool hasOverlap = false;
				for (int x = indentX; x < indentX + width && !hasOverlap; x++)
				{
					for (int y = indentY; y < indentY + height && !hasOverlap; y++)
					{
						// Check each cell for single coverage
						if (coverage[x][y] > 1)
						{
							hasOverlap = true;
						}
					}
				}
				if (!hasOverlap)
				{
					Console.WriteLine("Part 2: " + id);
					break;
				}
			}
		}
	}
}