﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
    class TimeConversion
    {
        public static void Main()
        {
            string[] timeSections = Console.ReadLine().Split(':');

            bool isPm = timeSections[2].Substring(2) == "PM";

            int hoursAsInt = Convert.ToInt32(timeSections[0]);

            if(isPm)
            {
                // Add 12 hours, unless if it's noon
                if (hoursAsInt != 12)
                {
                    hoursAsInt += 12;
                }
            }
            else
            {
                // Set midnight to 0h
                if(hoursAsInt == 12)
                {
                    hoursAsInt = 0;
                }
            }

            string timeAs24h = hoursAsInt.ToString("D2") + ":" + timeSections[1] + ":" + timeSections[2].Substring(0, 2);
            Console.WriteLine(timeAs24h);
        }
    }
}