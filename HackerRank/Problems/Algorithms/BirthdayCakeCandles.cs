﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HackerRank.Problems.Algorithms
{
	class BirthdayCakeCandles
	{
		public static void Main()
		{
			int numberOfCandles = Convert.ToInt32(Console.ReadLine());
			var candlesAsString = Console.ReadLine().Split(' ');

			var candles = new List<int>();
			foreach (var candleAsString in candlesAsString)
			{
				candles.Add(Convert.ToInt32(candleAsString));
			}

			var tallestCandle = candles.Max();
			var numberOfTallestCandle = candles.Where(x => x == tallestCandle).Count();
			Console.WriteLine(numberOfTallestCandle);
		}
	}
}