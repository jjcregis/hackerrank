﻿using System;

namespace HackerRank.AdventOfCode.Year2019
{
	class _1202ProgramAlarm
	{
		public static void Main()
		{
			int[] program;
			string input = @"";

			void ResetMemory()
			{
				string[] intCodesAsString = input.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
				program = Array.ConvertAll<string, int>(intCodesAsString, int.Parse);
			}

			void Add(int index)
			{
				int indexA = program[index + 1];
				int indexB = program[index + 2];
				int indexResult = program[index + 3];
				
				program[indexResult] = program[indexA] + program[indexB];
			}

			void Multiply(int index)
			{
				int indexA = program[index + 1];
				int indexB = program[index + 2];
				int indexResult = program[index + 3];
				
				program[indexResult] = program[indexA] * program[indexB];
			}

			int RunProgram(int noun, int verb)
			{
				program[1] = noun;
				program[2] = verb;

				bool halted = false;
				for (int i = 0; i < program.Length && !halted; i += 4)
				{
					switch (program[i])
					{
						case 1:
							Add(i);
							break;
						case 2:
							Multiply(i);
							break;
						case 99:
							halted = true;
							return program[0];
						default:
							throw new Exception("Something went wrong.");
					}
				}

				throw new Exception("Program did not halt.");
			}

			ResetMemory();
			int output = RunProgram(12, 2);
			Console.WriteLine($"Part 1: {output}");

			bool finished = false;
			for (int i = 0; i <= 99 && !finished; i++)
			{
				for (int j = 0; j <= 99 && !finished; j++)
				{
					ResetMemory();
					output = RunProgram(i, j);

					if (output == 19690720)
					{
						Console.WriteLine($"Part 2: {100 * i + j}");
					}
				}
			}
		}
	}
}
