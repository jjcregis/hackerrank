﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
	class SimpleArraySum
	{
		public static void Main()
		{
			Console.ReadLine();
            string[] numbersAsStrings = Console.ReadLine().Split(' ');
            int sum = 0;

            foreach (string number in numbersAsStrings)
            {
                sum += Convert.ToInt32(number);
            }

			Console.WriteLine(sum);
		}
	}
}