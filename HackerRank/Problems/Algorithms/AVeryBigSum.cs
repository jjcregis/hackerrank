﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
    class AVeryBigSum
    {
        public static void Main()
        {
            Console.ReadLine();
            string[] numbersAsStrings = Console.ReadLine().Split(' ');
            long sum = 0;

            foreach (string number in numbersAsStrings)
            {
                sum += Convert.ToInt64(number);
            }

            Console.WriteLine(sum);
        }
    }
}