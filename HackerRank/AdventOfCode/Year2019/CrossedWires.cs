﻿using System;
using System.Collections.Generic;

namespace HackerRank.AdventOfCode.Year2019
{
	class CrossedWires
	{
		enum Direction
		{
			Up,
			Down,
			Left,
			Right
		}

		[Flags]
		enum IntersectionState
		{
			None,
			First,
			Second,
			Both
		}

		struct Command
		{
			public Direction Direction { get; set; }
			public int Magnitude { get; set; }
		}

		public static void Main()
		{
			string input = @"";
			List<List<Command>> wires = new List<List<Command>>();

			// Read input
			string[] lines = input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
			foreach (string wireAsString in lines)
			{
				List<Command> newWire = new List<Command>();

				string[] commandsAsString = wireAsString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
				foreach (string commandAsString in commandsAsString)
				{
					Direction direction;
					switch (commandAsString[0])
					{
						case 'U':
							direction = Direction.Up;
							break;
						case 'D':
							direction = Direction.Down;
							break;
						case 'L':
							direction = Direction.Left;
							break;
						case 'R':
							direction = Direction.Right;
							break;
						default:
							throw new Exception($"Unknown command {commandAsString[0]}");
					}

					int magnitude = int.Parse(commandAsString.Substring(1));

					Command newCommand = new Command()
					{
						Direction = direction,
						Magnitude = magnitude
					};
					newWire.Add(newCommand);

				}
				wires.Add(newWire);
			}

			// Find the size of the solution space and get the starting position before solving the problem
			int minX = 0;
			int minY = 0;
			int maxX = 0;
			int maxY = 0;

			foreach (var wire in wires)
			{
				var cursor = (y: 0, x: 0);
				foreach (var command in wire)
				{
					// Move the cursor
					switch (command.Direction)
					{
						case Direction.Up:
							cursor.y += command.Magnitude;
							break;
						case Direction.Down:
							cursor.y -= command.Magnitude;
							break;
						case Direction.Left:
							cursor.x -= command.Magnitude;
							break;
						case Direction.Right:
							cursor.x += command.Magnitude;
							break;
					}

					// Check if the solution space has grown
					minX = Math.Min(minX, cursor.x);
					minY = Math.Min(minY, cursor.y);
					maxX = Math.Max(maxX, cursor.x);
					maxY = Math.Max(maxY, cursor.y);
				}
			}

			var size = (x: Math.Abs(maxX - minX) + 1, y: Math.Abs(maxY - minY) + 1);
			var startPosition = (x: -minX, y: -minY);

			IntersectionState[][] space = new IntersectionState[size.x][];
			for (int i = 0; i < size.x; i++)
			{
				space[i] = new IntersectionState[size.y];
			}

			HashSet<(int x, int y)> intersections = new HashSet<(int, int)>();

			// Lay the wires and track intersections
			for (int i=0; i<wires.Count; i++)
			{
				var wire = wires[i];
				var lastStopPosition = startPosition;
				IntersectionState wireState = i == 0 ? IntersectionState.First : IntersectionState.Second;

				for(int j=0; j< wire.Count; j++)
				{
					Command command = wire[j];
					int direction = command.Direction == Direction.Up || command.Direction == Direction.Right ? 1 : -1;

					switch(command.Direction)
					{
						case Direction.Up:
						case Direction.Down:
							for(int k=0; k<command.Magnitude; k++)
							{
								space[lastStopPosition.x][lastStopPosition.y + k * direction] |= wireState;
								if (space[lastStopPosition.x][lastStopPosition.y + k * direction] == IntersectionState.Both)
								{
									var coordinates = (lastStopPosition.x, lastStopPosition.y + k * direction);
									intersections.Add(coordinates);
								}
							}
							lastStopPosition = (lastStopPosition.x, lastStopPosition.y + command.Magnitude * direction);
							break;
						case Direction.Left:
						case Direction.Right:
							for (int k = 0; k < command.Magnitude; k++)
							{
								space[lastStopPosition.x + k * direction][lastStopPosition.y] |= wireState;
								if (space[lastStopPosition.x + k * direction][lastStopPosition.y] == IntersectionState.Both)
								{
									var coordinates = (lastStopPosition.x + k * direction, lastStopPosition.y);
									intersections.Add(coordinates);
								}
							}
							lastStopPosition = (lastStopPosition.x + command.Magnitude * direction, lastStopPosition.y);
							break;
					}
				}
			}

			// Find the closest intersection to the starting position
			var closestDistance = int.MaxValue;
			foreach (var intersection in intersections)
			{
				int distance = Math.Abs(intersection.x - startPosition.x) + Math.Abs(intersection.y - startPosition.y);
				if (distance != 0)
				{
					closestDistance = Math.Min(closestDistance, distance);
				}
			}
			Console.WriteLine($"Part 1: {closestDistance}");

			// For each wire, track how many steps it takes to reach each intersection
			Dictionary<(int x, int y), int> stepsToIntersections = new Dictionary<(int x, int y), int>();
			for (int i = 0; i < wires.Count; i++)
			{
				var wire = wires[i];
				var lastStopPosition = startPosition;
				int steps = 0;

				for (int j = 0; j < wire.Count; j++)
				{
					Command command = wire[j];
					int direction = command.Direction == Direction.Up || command.Direction == Direction.Right ? 1 : -1;

					switch (command.Direction)
					{
						case Direction.Up:
						case Direction.Down:
							for (int k = 0; k < command.Magnitude; k++)
							{
								if (space[lastStopPosition.x][lastStopPosition.y + k * direction] == IntersectionState.Both)
								{
									var coordinates = (lastStopPosition.x, lastStopPosition.y + k * direction);
									intersections.Add(coordinates);
									if(!stepsToIntersections.ContainsKey(coordinates))
									{
										stepsToIntersections.Add(coordinates, 0);
									}
									stepsToIntersections[coordinates] += steps;
								}
								steps++;
							}
							lastStopPosition = (lastStopPosition.x, lastStopPosition.y + command.Magnitude * direction);
							break;
						case Direction.Left:
						case Direction.Right:
							for (int k = 0; k < command.Magnitude; k++)
							{
								if (space[lastStopPosition.x + k * direction][lastStopPosition.y] == IntersectionState.Both)
								{
									var coordinates = (lastStopPosition.x + k * direction, lastStopPosition.y);
									intersections.Add(coordinates);
									intersections.Add(coordinates);
									if (!stepsToIntersections.ContainsKey(coordinates))
									{
										stepsToIntersections.Add(coordinates, 0);
									}
									stepsToIntersections[coordinates] += steps;
								}
								steps++;
							}
							lastStopPosition = (lastStopPosition.x + command.Magnitude * direction, lastStopPosition.y);
							break;
					}
				}
			}

			// Find which intersection has the smallest sum of steps
			int fewestSteps = int.MaxValue;
			foreach(var stepsToIntersection in stepsToIntersections)
			{
				if (stepsToIntersection.Value != 0)
				{
					fewestSteps = Math.Min(fewestSteps, stepsToIntersection.Value);
				}
			}
			Console.WriteLine($"Part 2: {fewestSteps}");
		}
	}
}
