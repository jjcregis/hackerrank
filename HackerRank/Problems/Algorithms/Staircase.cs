﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
    class Staircase
    {
        public static void Main()
        {
            int staircaseSize = Convert.ToInt32(Console.ReadLine());
            for(int i=0; i<staircaseSize; i++)
            {
                string stepString = string.Empty;

                for (int j = 0; j < staircaseSize; j++)
                {
                    if (j < staircaseSize - i - 1)
                    {
                        stepString += " ";
                    }
                    else
                    {
                        stepString += "#";
                    }
                }
                Console.WriteLine(stepString);
            }
        }
    }
}