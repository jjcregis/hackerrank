﻿using System;

namespace HackerRank.Problems.Algorithms
{
    class SherlockAndSquares
    {
        private struct Range
        {
            public int min;
            public int max;
        }

        public static void Main()
        {
            int numberOfTestCases = Convert.ToInt32(Console.ReadLine());
            Range[] ranges = new Range[numberOfTestCases];

            for (int i = 0; i < numberOfTestCases; i++)
            {
                string[] testCaseInfoAsString = Console.ReadLine().Split(' ');
                ranges[i].min = Convert.ToInt32(testCaseInfoAsString[0]);
                ranges[i].max = Convert.ToInt32(testCaseInfoAsString[1]);
            }

            for (int i = 0; i < ranges.Length; i++)
            {
                Range range = ranges[i];

                // Get starting point
                int root = (int)Math.Sqrt(range.min);
                int count = 0;

                while (root*root <= range.max)
                {
                    int square = root * root;
                    if (square >= range.min && square <= range.max)
                    {
                        count++;
                    }
                    root++;
                }

                Console.WriteLine(count);
            }
        }
    }
}