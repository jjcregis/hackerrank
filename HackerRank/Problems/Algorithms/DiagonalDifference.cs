﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
    class DiagonalDifference
    {
        public static void Main()
        {
            int matrixSize = Convert.ToInt32(Console.ReadLine());

            // Read the map (cells[row][col])
            string[][] cells = new string[matrixSize][];
            for (int i = 0; i < matrixSize; i++)
            {
                cells[i] = Console.ReadLine().Split(' ');
            }

            // Calculate the diagonal difference
            int sum1 = 0;
            int sum2 = 0;
            for (int i = 0; i < matrixSize; i++)
            {
                sum1 += Convert.ToInt32(cells[i][i]);
                sum2 += Convert.ToInt32(cells[i][matrixSize - 1 - i]);
            }

            Console.WriteLine(Math.Abs(sum1 - sum2));
        }
    }
}