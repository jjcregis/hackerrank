﻿using System;
using System.Collections.Generic;

namespace HackerRank.AdventOfCode.Year2019
{
	class SecureContainer
	{
		public static void Main()
		{
			bool DigitsOnlyIncrease(int number)
			{
				bool valid = true;
				int? lastDigit = null;
				while (number > 0)
				{
					int digit = number % 10;
					if (lastDigit != null && digit > lastDigit)
					{
						valid = false;
						break;
					}
					lastDigit = digit;
					number /= 10;
				}

				return valid;
			}

			bool ContainsGroupOfTwo(int number)
			{
				int i = number;
				bool valid = false;

				// Check for group of two digits
				int? lastDigit = null;
				int streak = 1;
				while (number > 0)
				{
					int digit = number % 10;
					if (lastDigit != null)
					{
						if (digit == lastDigit)
						{
							streak++;							
							if (streak == 2 && number < 10)
							{
								// Streak ended at the final digits
								valid = true;
								break;
							}
						}
						else
						{
							if (streak == 2)
							{
								// Streak ended before the final digits
								valid = true;
								break;
							}
							streak = 1;
						}						
					}

					// Move to the next digit
					lastDigit = digit;
					number /= 10;
				}

				return valid;
			}

			string input = @"";

			// Read input
			string[] range = input.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
			int min = int.Parse(range[0]);
			int max = int.Parse(range[1]);
			List<int> possibilities = new List<int>();

			for (int i=min; i<=max; i++)
			{
				int password = i;
				bool valid = true;

				// Check for adjacent digits
				int? lastDigit = null;
				while (password > 0)
				{
					int digit = password % 10;
					if (lastDigit != null && digit == lastDigit)
					{
						break;
					}
					lastDigit = digit;
					password /= 10;
					if (password <= 0)
					{
						valid = false;
					}
				}

				if(!valid)
				{
					continue;
				}

				// Check for no decreasing digits
				valid = DigitsOnlyIncrease(i);
				if(!valid)
				{
					continue;
				}

				if (valid)
				{
					possibilities.Add(i);
				}
			}

			Console.WriteLine($"Part 1: {possibilities.Count}");
			
			List<int> newPossibilities = new List<int>();
			foreach(int possibility in possibilities)
			{
				// Check for group of two digits
				bool valid = ContainsGroupOfTwo(possibility);
				if (valid)
				{
					newPossibilities.Add(possibility);
				}
			}

			Console.WriteLine($"Part 2: {newPossibilities.Count}");
		}
	}
}
