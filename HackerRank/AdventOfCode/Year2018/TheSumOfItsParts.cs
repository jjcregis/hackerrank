﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HackerRank.AdventOfCode.Year2018
{
	class TheSumOfItsParts
	{
		private class WorkerStep
		{
			public char Step;
			public int TimeLeft;
			public WorkerStep(char step, int timeLeft)
			{
				Step = step;
				TimeLeft = timeLeft;
			}
		}

		public static void Main()
		{
			string input = @"";

			// Add all steps, initialised with no dependants
			string steps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			Dictionary<char, HashSet<char>> stepsAndDependants = new Dictionary<char, HashSet<char>>();
			foreach(char step in steps)
			{
				stepsAndDependants.Add(step, new HashSet<char>());
			}

			// Read input
			string[] lines = input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
			foreach(string line in lines)
			{
				string[] lineTokens = line.Split(' ');
				HashSet<char> dependants;
				char firstStep = lineTokens[1][0];
				char secondStep = lineTokens[7][0];
				
				dependants = stepsAndDependants[secondStep];
				dependants.Add(firstStep);
				stepsAndDependants[secondStep] = dependants;
			}

			// Create a copy for Part 2
			Dictionary<char, HashSet<char>> stepsAndDependantsPart2 = new Dictionary<char, HashSet<char>>();
			foreach (char step in stepsAndDependants.Keys)
			{
				stepsAndDependantsPart2.Add(step, new HashSet<char>(stepsAndDependants[step]));
			}

			string stepOrder = "";
			SortedSet<char> sortedSet = new SortedSet<char>();
			do
			{
				// Find all steps that can be completed and sort them
				foreach (char step in stepsAndDependants.Keys)
				{
					if (stepsAndDependants[step].Count == 0)
					{
						sortedSet.Add(step);
					}
				}

				// Complete a step
				char completedStep = sortedSet.First();
				sortedSet.Remove(completedStep);
				stepsAndDependants.Remove(completedStep);
				stepOrder += completedStep;

				foreach(char step in stepsAndDependants.Keys)
				{
					// Remove the dependency from all other steps
					if(stepsAndDependants[step].Contains(completedStep))
					{
						stepsAndDependants[step].Remove(completedStep);
					}
				}
			}
			while (stepsAndDependants.Count > 0);
			Console.WriteLine("Part 1: " + stepOrder);

			const int WORKER_COUNT = 5;
			WorkerStep[] workers = new WorkerStep[WORKER_COUNT];

			// Find all steps that can be completed and sort them
			sortedSet = new SortedSet<char>();
			int timeStep = 0;
			bool workersInProgress = false;

			while(stepsAndDependantsPart2.Count > 0 || workersInProgress)
			{
				workersInProgress = false;

				// Find all steps that can be completed and sort them
				foreach (char step in stepsAndDependantsPart2.Keys)
				{
					if (stepsAndDependantsPart2[step].Count == 0)
					{
						sortedSet.Add(step);
					}
				}

				// Assign available steps to available workers
				for (int i = 0; i < workers.Length; i++)
				{
					WorkerStep worker = workers[i];
					if (worker == null && sortedSet.Count > 0)
					{
						// Start on a step
						char stepInProgress = sortedSet.First();
						sortedSet.Remove(stepInProgress);
						stepsAndDependantsPart2.Remove(stepInProgress);

						// Add the time for the worker
						workers[i] = new WorkerStep(stepInProgress, 60 + steps.IndexOf(stepInProgress) + 1);
					}
				}

				// Perform work on steps
				for (int i=0; i< workers.Length; i++)
				{
					if (workers[i] == null)
					{
						// Worker is idle
						continue;
					}
					if (workers[i].TimeLeft > 0)
					{
						workers[i].TimeLeft--;
					}
					if (workers[i].TimeLeft > 0)
					{
						workersInProgress = true;
					}
				}

				// Clean up finished workers and completed steps
				for (int i = 0; i < workers.Length; i++)
				{
					WorkerStep worker = workers[i];
					if (worker != null && worker.TimeLeft <= 0)
					{
						foreach (char step in stepsAndDependantsPart2.Keys)
						{
							// Remove the dependency from all other steps
							if (stepsAndDependantsPart2[step].Contains(worker.Step))
							{
								stepsAndDependantsPart2[step].Remove(worker.Step);
							}
						}
						workers[i] = null;
					}
				}
				
				// Advance time
				timeStep++;
			}
			Console.WriteLine("Part 2: " + (timeStep));
		}
	}
}