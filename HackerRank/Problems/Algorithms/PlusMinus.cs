﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
    class PlusMinus
    {
        public static void Main()
        {
            int numberOfInts = Convert.ToInt32(Console.ReadLine());
            string[] numbersAsStrings = Console.ReadLine().Split(' ');
            int positiveCount = 0;
            int zeroCount = 0;
            int negativeCount = 0;

            foreach (string numberAsString in numbersAsStrings)
            {
                int number = Convert.ToInt32(numberAsString);
                if (number > 0)
                {
                    positiveCount++;
                }
                else if (number == 0)
                {
                    zeroCount++;
                }
                else
                {
                    negativeCount++;
                }
            }

            Console.WriteLine(((float)positiveCount / (float)numberOfInts).ToString("F3"));
            Console.WriteLine(((float)negativeCount / (float)numberOfInts).ToString("F3"));
            Console.WriteLine(((float)zeroCount / (float)numberOfInts).ToString("F3"));
        }
    }
}