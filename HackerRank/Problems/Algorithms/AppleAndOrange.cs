﻿using System;
using System.Linq;

namespace HackerRank.Problems.Algorithms
{
	class AppleAndOrange
	{
		public static void Main()
		{
			string[] stringInput = Console.ReadLine().Split(' ');
			var housePoints = (Convert.ToInt32(stringInput[0]), Convert.ToInt32(stringInput[1]));

			stringInput = Console.ReadLine().Split(' ');
			int applePoint = Convert.ToInt32(stringInput[0]);
			int orangePoint = Convert.ToInt32(stringInput[1]);

			stringInput = Console.ReadLine().Split(' ');
			int numberOfApples = Convert.ToInt32(stringInput[0]);
			var numberOfOranges = Convert.ToInt32(stringInput[1]);

			var appleDistances = Console.ReadLine().Split(' ').Select(x => Convert.ToInt32(x));
			var orangeDistances = Console.ReadLine().Split(' ').Select(x => Convert.ToInt32(x));

			Func<int, bool> IsOnHouse = position => position >= housePoints.Item1 && position <= housePoints.Item2;

			var applesOnHouse = appleDistances.Where(x => IsOnHouse(x + applePoint)).Count();
			var orangesOnHouse = orangeDistances.Where(x => IsOnHouse(x + orangePoint)).Count();

			Console.WriteLine(applesOnHouse);
			Console.WriteLine(orangesOnHouse);
		}
	}
}