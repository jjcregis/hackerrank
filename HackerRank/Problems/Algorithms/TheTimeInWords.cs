﻿using System;

namespace HackerRank.Problems.Algorithms
{
    class TheTimeInWords
    {
        public static void Main()
        {
            string[] timeAsWords = new string[] { "o' clock", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
                "ten", "eleven", "twelve", "thirteen", "fourteen", "quarter", "sixteen", "seventeen", "eighteen", "nineteen",
                "twenty", "twenty one", "twenty two", "twenty three", "twenty four", "twenty five", "twenty six", "twenty seven", "twenty eight", "twenty nine", "half" };

            int hours = Convert.ToInt32(Console.ReadLine());
            int minutes = Convert.ToInt32(Console.ReadLine());

            // On the hour
            if (minutes == 0)
            {
                Console.WriteLine(timeAsWords[hours] + " " + timeAsWords[minutes]);
            }

            else
            {
                string units = " minutes ";
                if (minutes == 15 || minutes == 30 || minutes == 45)
                {
                    units = " ";
                }
                else if (minutes == 1)
                {
                    units = " minute ";
                }

                bool isPastThirty = minutes > 30;
                string relation = isPastThirty ? "to" : "past";
                int hoursToPrint = isPastThirty ? hours + 1 : hours;
                int minutesToPrint = isPastThirty ? 60 - minutes : minutes;

                Console.WriteLine(timeAsWords[minutesToPrint] + units + relation + " " + timeAsWords[hoursToPrint]);
            }
        }
    }
}