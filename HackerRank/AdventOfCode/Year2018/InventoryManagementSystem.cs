﻿using System;
using System.Collections.Generic;

namespace HackerRank.AdventOfCode.Year2018
{
	class InventoryManagementSystem
	{
		public static void Main()
		{
			string input = @"";

			string[] splitInput = input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
			int containing2Count = 0;
			int containing3Count = 0;

			foreach (string line in splitInput)
			{
				Dictionary<char, int> letterCounts = new Dictionary<char, int>();
				
				// Get total number of each digit
				foreach(char c in line)
				{
					if (!letterCounts.ContainsKey(c))
					{
						letterCounts.Add(c, 0);
					}
					letterCounts[c]++;
				}

				// Check if there are exactly two or three of a digit
				bool contains2 = false;
				bool contains3 = false;
				foreach(int count in letterCounts.Values)
				{
					if (count == 2 && !contains2)
					{
						containing2Count++;
						contains2 = true;
					}

					if (count == 3 && !contains3)
					{
						containing3Count++;
						contains3 = true;
					}
				}

			}
			Console.WriteLine("Part 1: " + containing2Count * containing3Count);

			// Check if two lines differ by exactly one character
			for (int i=0; i<splitInput.Length; i++)
			{
				for(int j=i; j<splitInput.Length; j++)
				{
					int differentCount = 0;
					int differentDigitIndex = 0;
					for(int k=0; k<splitInput[i].Length; k++)
					{
						if(splitInput[i][k] != splitInput[j][k])
						{
							differentCount++;
							differentDigitIndex = k;
						}
					}
					if (differentCount == 1)
					{
						string commonLetters = string.Empty;
						if (differentDigitIndex > 0)
						{
							commonLetters += splitInput[i].Substring(0, differentDigitIndex);
						}
						if(differentDigitIndex < splitInput[i].Length - 1)
						{
							commonLetters += splitInput[i].Substring(differentDigitIndex + 1);
						}
						Console.WriteLine("Part 2: " + commonLetters);
						break;
					}
				}
			}
		}
	}
}