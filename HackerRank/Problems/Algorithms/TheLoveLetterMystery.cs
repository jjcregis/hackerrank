﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
	class TheLoveLetterMystery
	{
		public static void Main()
		{
			int numberOfTestCases = Convert.ToInt32(Console.ReadLine());
			for(int i=0; i<numberOfTestCases; i++)
			{
				string line = Console.ReadLine();
				int numberOfOperations = 0;

				for(int j=0; j<line.Length / 2; j++)
				{
					numberOfOperations += Math.Abs(line[j] - line[line.Length - j - 1]);
				}

				Console.WriteLine(numberOfOperations);
			}
		}
	}
}