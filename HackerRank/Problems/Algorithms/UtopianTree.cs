﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
    class UtopianTree
    {
		public static void Main()
        {
            int numberOfTestCases = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < numberOfTestCases; i++)
            {
                int numberOfGrowthCycles = Convert.ToInt32(Console.ReadLine());
                int height = 1;

                for (int j = 0; j < numberOfGrowthCycles; j++)
                {
                    if (j % 2 == 0)
                    {
                        height *= 2;
                    }
                    else
                    {
                        height++;
                    }
                }

                Console.WriteLine(height);
            }
        }
    }
}