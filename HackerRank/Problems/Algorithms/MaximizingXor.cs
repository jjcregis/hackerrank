﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
    class MaximizingXor
    {
		public static void Main()
        {
            uint l = Convert.ToUInt32(Console.ReadLine());
            uint r = Convert.ToUInt32(Console.ReadLine());
            uint maxValue = 0;


			for (uint a = l; a <= r; a++)
			{
				for (uint b = a; b <= r; b++)
				{
					uint xor = a ^ b;
					if (xor > maxValue)
					{
						maxValue = xor;
					}

					l++;
				}
			}

            Console.WriteLine(maxValue);
        }
    }
}