﻿using System;

namespace HackerRank.Problems.Algorithms
{
    class AppendAndDelete
    {
        public static void Main()
        {
            string stringA = Console.ReadLine();
            string stringB = Console.ReadLine();
            int numberOfOps = Convert.ToInt32(Console.ReadLine());

            // Find the largest common string
            int commonStringLength = 0;
            for(int i = 0; i<stringA.Length && i<stringB.Length; i++)
            {
                if(stringA[i] != stringB[i])
                {
                    break;
                }
                commonStringLength++;
            }

            // Compare remaining substring lengths
            int numberOfDifferentCharacters = stringA.Length + stringB.Length - 2 * commonStringLength;

            // Check if number of ops is enough to make strings equal
            if (numberOfOps - numberOfDifferentCharacters >= 0)
            {
                // Mod 2 accounts for appends and deletes that cancel each other out
                // Other case covers total string deletions and doing further deletes on empty string
                if ((numberOfOps - numberOfDifferentCharacters) % 2 == 0 ||
                    numberOfOps - stringA.Length >= stringB.Length)
                {
                    Console.WriteLine("Yes");
                }
                else
                {
                    Console.WriteLine("No");
                }
            }
            else
            {
                Console.WriteLine("No");
            }
        }
    }
}