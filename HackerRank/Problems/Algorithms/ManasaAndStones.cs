﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
	class ManasaAndStones
	{
		public static void Main()
		{
			int numberOfTestCases = Convert.ToInt32(Console.ReadLine());

			for (int i = 0; i < numberOfTestCases; i++)
			{
				int numberOfStones = Convert.ToInt32(Console.ReadLine());
				int a = Convert.ToInt32(Console.ReadLine());
				int b = Convert.ToInt32(Console.ReadLine());
				HashSet<int> possibilities = new HashSet<int>();
				int numberOfDifferences = numberOfStones - 1;

				for (int j = 0; j < numberOfStones; j++)
				{
					possibilities.Add(a * (j) + b * (numberOfDifferences - j));
				}

				// Sort from lowest to highest
				SortedSet<int> sortedPossibilities = new SortedSet<int>();
				var enumerator = possibilities.GetEnumerator();
				while (enumerator.MoveNext())
				{
					sortedPossibilities.Add(enumerator.Current);
				}	
				sortedPossibilities.Reverse();

				// Add output to string
				string answerString = string.Empty;
				foreach (int number in sortedPossibilities)
				{
					answerString += number + " ";
				}

				// Remove trailing space
				answerString = answerString.Remove(answerString.Length - 1);

				Console.WriteLine(answerString);
			}
		}
	}
}