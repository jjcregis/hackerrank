﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HackerRank.Problems.Algorithms
{
    class CavityMap
    {
        public static void Main()
        {
            int mapSize = Convert.ToInt32(Console.ReadLine());
            List<Tuple<int, int>> cavities = new List<Tuple<int, int>>();

            // Read the map (cells[row][col])
            char[][] cells = new char[mapSize][];
            for (int i = 0; i < mapSize; i++)
            {
                cells[i] = Console.ReadLine().ToCharArray();
            }

            // Scan for cavities
            for (int i = 1; i < mapSize - 1; i++)
            {
                for (int j = 1; j < mapSize - 1; j++)
                {
                    char depth = cells[j][i];

                    if (depth > cells[j - 1][i] &&
                        depth > cells[j + 1][i] &&
                        depth > cells[j][i - 1] &&
                        depth > cells[j][i + 1])
                    {
                        cavities.Add(new Tuple<int, int>(i, j));
                    }
                }
            }

            // Replace cavities with X's
            foreach (Tuple<int,int> cavity in cavities)
            {
                int row = cavity.Item2;
                cells[cavity.Item2][cavity.Item1] = 'X';
            }

            // Print output
            foreach (char[] row in cells)
            {
                Console.WriteLine(row);
            }
        }
    }
}