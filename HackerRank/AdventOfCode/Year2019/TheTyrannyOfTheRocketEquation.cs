﻿using System;

namespace HackerRank.AdventOfCode.Year2019
{
	class TheTyrannyOfTheRocketEquation
	{
		public static int MassToFuel(int mass)
		{
			int fuel = mass / 3 - 2;
			return fuel > 0 ? fuel : 0;
		}

		public static void Main()
		{
			string input = @"";

			// Read input
			string[] lines = input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

			int fuelRequired = 0;
			foreach(string line in lines)
			{
				int.TryParse(line, out int mass);
				fuelRequired += MassToFuel(mass);
			}
			Console.WriteLine($"Part 1: {fuelRequired}");

			fuelRequired = 0;
			foreach (string line in lines)
			{
				int.TryParse(line, out int mass);
				int massToConsider = mass;

				do
				{
					massToConsider = MassToFuel(massToConsider);
					fuelRequired += massToConsider;
				}
				while (massToConsider > 0);
			}
			Console.WriteLine($"Part 2: {fuelRequired}");
		}
	}
}
