﻿using System;
using System.Collections.Generic;

namespace HackerRank.AdventOfCode.Year2019
{
	class UniversalOrbitMap
	{
		public static void Main()
		{
			// Recursively find how many direct and indirect orbits a body has
			int GetOrbitsCount(string orbiter, ref Dictionary<string, List<string>> collection)
			{
				if (!collection.ContainsKey(orbiter) || collection[orbiter].Count == 0)
				{
					return 0;
				}
				else
				{
					int count = 0;
					foreach (string body in collection[orbiter])
					{
						count++;
						count += GetOrbitsCount(body, ref collection);
					}
					return count;
				}
			}

			List<string> GetPathToCOM(string position, ref Dictionary<string, List<string>> collection)
			{
				List<string> path = new List<string>();
				string currentPos = position;
				while (currentPos != "COM")
				{
					path.Add(currentPos);
					currentPos = collection[currentPos][0];
				}
				return path;
			}

			string input = @"";
			
			var pairs = new List<(string, string)>();

			// Read input
			string[] lines = input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
			foreach (string line in lines)
			{
				string[] pair = line.Split(new char[] { ')' }, StringSplitOptions.RemoveEmptyEntries);
				pairs.Add((pair[0], pair[1]));
			}
			
			// Get what each body is orbitting
			var bodyIsOrbiting = new Dictionary<string, List<string>>();
			foreach (var pair in pairs)
			{
				var body = pair.Item1;
				var orbiter = pair.Item2;

				if (!bodyIsOrbiting.ContainsKey(orbiter))
				{
					bodyIsOrbiting.Add(orbiter, new List<string>());
				}
				bodyIsOrbiting[orbiter].Add(body);
			}

			// Find the total number of direct and indirect orbits per body
			var totalOrbitCounts = new Dictionary<string, int>();
			foreach(var pair in pairs)
			{
				var orbiter = pair.Item2;
				if(!totalOrbitCounts.ContainsKey(orbiter))
				{
					totalOrbitCounts[orbiter] = GetOrbitsCount(orbiter, ref bodyIsOrbiting);
				}
			}

			// Add all of the orbit counts
			int totalOrbits = 0;
			foreach(var total in totalOrbitCounts.Values)
			{
				totalOrbits += total;
			}
			Console.WriteLine($"Part 1: {totalOrbits}");


			// Get path back to COM from YOU and SAN
			List<string> youPath = GetPathToCOM(bodyIsOrbiting["YOU"][0], ref bodyIsOrbiting);
			List<string> sanPath = GetPathToCOM(bodyIsOrbiting["SAN"][0], ref bodyIsOrbiting);

			// Reverse paths to get the path from COM to the bodies
			youPath.Reverse();
			sanPath.Reverse();

			// Find where the paths intersect
			int intersectionIndex = int.MinValue;
			for (int i=0; i<youPath.Count && i< sanPath.Count; i++)
			{
				if(youPath[i] != sanPath[i])
				{
					intersectionIndex = i;
					break;
				}
			}

			int pathLength = (youPath.Count - intersectionIndex) + (sanPath.Count - intersectionIndex);
			Console.WriteLine($"Part 2: {pathLength}");
		}
	}
}
